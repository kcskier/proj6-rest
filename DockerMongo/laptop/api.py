from flask import Flask
import flask
from flask import request
from flask_restful import Resource, Api
import json
from pymongo import *
import config

app = Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

#Some Variables for Mongo, copied over from flask_brevets.py
client = MongoClient('172.25.0.2', 27017)
db = client["brevetdb"]
collection = db["brevetdb"]

#Part I
#Calls get_times with default values: typ = both, num = 20, form = json 
class Listall(Resource):
    def get(self):
        return flask.jsonify(get_times())

#Calls get_times with default values: typ = close, num = 20, form = json 
class ListOpen(Resource):
    def get(self):
        return flask.jsonify(get_times(typ = 'open'))

#Calls get_times with default values: typ = open, num = 20, form = json 
class ListClose(Resource):
    def get(self):
        return flask.jsonify(get_times(typ = 'close'))
    
#PART II & PART III
#Same as Listall, but explicitly JSON
class ListallJSON(Resource):
    def get(self):
        return flask.jsonify(get_times())
    
#Same as ListOpen, but explicitly JSON, can take in a value for 'top'
class ListOpenJSON(Resource):
    def get(self):
        x = 20
        try:
            if int(request.args['top']) <= 20:
                x = int(request.args['top'])
        except:
            x = 20
        return flask.jsonify(get_times(typ = 'open', num = x))
    
#Same as ListClose, but explicitly JSON, can take in a value for 'top'
class ListCloseJSON(Resource):
    def get(self):
        x = 20
        try:
            if int(request.args['top']) <= 20:
                x = int(request.args['top'])
        except:
            x = 20
        return flask.jsonify(get_times(typ = 'close', num = x))
    
#Same as Listall, but in CSV format
class ListallCSV(Resource):
    def get(self):
        return get_times(form = 'csv')
    
#Same as ListOpen, but in CSV format, can take in a value for 'top'
class ListOpenCSV(Resource):
    def get(self):
        x = 20
        try:
            if int(request.args['top']) <= 20:
                x = int(request.args['top'])
        except:
            x = 20
        return get_times(typ = 'open', num = x, form = 'csv')
    
#Same as ListClose, but in CSV format, can take in a value for 'top'
class ListCloseCSV(Resource):
    def get(self):
        x = 20
        try:
            if int(request.args['top']) <= 20:
                x = int(request.args['top'])
        except:
            x = 20
        return get_times(typ = 'close', num = x, form = 'csv')
    

#PART I
#Basic list APIs (JSON Default)
api.add_resource(Listall, '/listAll')
api.add_resource(ListOpen, '/listOpenOnly')
api.add_resource(ListClose, '/listCloseOnly')

#PART II & PART III
#Same as above, but they explicitly ask for JSON
api.add_resource(ListallJSON, '/listAll/json')

#These also can take in a value for 'top'
api.add_resource(ListOpenJSON, '/listOpenOnly/json')
api.add_resource(ListCloseJSON, '/listCloseOnly/json')

#Same as above, but in CSV format
api.add_resource(ListallCSV, '/listAll/csv')

#These also can take in a value for 'top'
api.add_resource(ListOpenCSV, '/listOpenOnly/csv')
api.add_resource(ListCloseCSV, '/listCloseOnly/csv')

'''
Since all of the URIs are just looking for the same data in different formats, we can create a single function
to do most of the work for us.
HOW THIS WORKS:
get_times takes in three variables:
    typ is the type of time you want (open/close/both)
    num is how many down the list you want (should be a number 1 - 20)
    form is the formating of the data (should be json or csv)
EXAMPLE CALL: get_times(open, 5, json)
    the above call should result in the function returning a json object, with the top 5 open times
'''
def get_times(typ = 'both', num = 20, form = 'json'):
    data = []
    i = 0
    for i in range(0, num):
        #finding the JSON Object
        obj = collection.find_one({'_id': i })
        
        #check if there is actually anything this particular DB entry. If not, we return data
        if obj['open'] == '':
            return format_csv(data)
        
        
        #Removing things we don't need, and formatting
        del obj["location"]
        del obj["km"]
        del obj["miles"]
        obj['_id'] = int(obj['_id'])
        
        #cases for each open/close calls
        if typ == 'close':
            del obj['open']
        if typ == 'open':
            del obj['close'] 
        data.append(obj)
        i = i + 1
        
    if form == 'csv':
        return format_csv(data)   
    else:
        return data

#for formatting CSV
def format_csv(data):
    str_data = str(data)
    str_data = str_data.replace('[','')
    str_data = str_data.replace(']','')
    str_data = str_data.replace('{','')
    str_data = str_data.replace('}','')
    str_data = str_data.replace('\"','')
    str_data = str_data.replace('\'','')
    return str_data
    

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=CONFIG.API_PORT)
